
// import React from 'react';
import ReactDOM from 'react-dom';
import './stylesheets/index.css';
import { Routes } from "./Routes.js";
// import App from './components/App';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(Routes, document.getElementById('root'));
registerServiceWorker();
