import React from 'react';
import { Link } from 'react-router-dom'


const Header = () => (
    <div>
      <nav className="navbar navbar-default">
        &nbsp;&nbsp;&nbsp;
      <Link to="/">home</Link>
        &nbsp;&nbsp;&nbsp;
      <Link to="/albums">albums</Link>
        &nbsp;&nbsp;&nbsp;
      <Link to="/about">about</Link>
        &nbsp;&nbsp;&nbsp;
    </nav>
    </div>
);

export default Header;