import React from "react";
import Header from "./Header"


export default class About extends React.Component {
    render() {
        return (
					<div>
						<Header />
						<div className="pure-g">
							<div className="pure-u-1-1">
								<h1><span className="small-print">surreal</span>REACT PhotoBrowser</h1>
							</div>
						</div>
						<div className="pure-g">
							<div className="pure-u-1-1">
								<p>
								This is a training spec for a photo browsing web application. The goal the application is to render a simple grid of images and allow navigation from each image to a details page.
								</p>
								<p>
								The application should use JSONPlaceholder as a source for the photo content
								</p>
							</div>
						</div>
					</div>
        );
    }
}