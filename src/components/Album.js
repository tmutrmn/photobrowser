import React from "react";
import { Link } from 'react-router-dom'
import Header from './Header'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { requestAlbum } from '../actions/Actions'




class Album extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.albumId
        };
    }
    
    componentDidMount() {
        this.props.requestAlbum(this.state.id)
    }
    
    image(image) {
        return (
            <div className="pure-u-1-4" key={image.id}>
                <figure>
                    <Link to={'/image/' + image.id }>
                        <img src={image.thumbnailUrl} key={image.id} alt={image.title} />
                        <figcaption>
                            <h4>{image.title}</h4>
                            <button className="button-c pure-button">See full image</button>
                        </figcaption>
                    </Link>
                </figure>
            </div>
        )
      
    }

    render() {
        // const { results = [] } = this.props.data
        // {(this.props.data.resuts || []).map(this.person)} , {results.map(this.person)}
        return ( 
            <div>
                <Header />
                <h1>Photos in album number: {this.state.id}</h1>
                <div className="pure-g">
                { this.props.data.length ? this.props.data.map(image => this.image(image)) : <pre>Loading images...</pre> }
                </div>
            </div>
        )
    }

}


const mapStateToProps = state => ({ id: state.id ,data: state.data })

const mapDispatchToProps = dispatch =>
    bindActionCreators({ requestAlbum }, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Album)







