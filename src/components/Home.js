import React from "react";
import Header from './Header'
import Albums from './Albums'


export default class Home extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <div className="page-home">
                    <h1><span className="small-print">surreal</span>REACT PhotoBrowser</h1>
                </div>
                <Albums />
            </div>
        );
    }
}