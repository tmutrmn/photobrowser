import React from "react";
import { Link } from 'react-router-dom'
import Header from "./Header"

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { requestAlbums } from '../actions/Actions'


class Albums extends React.Component {

    componentWillMount() {
		this.props.requestAlbums()
    }


    albumThumb = (img) => {
        return (
            <img src={img} alt="" className="albumThumbnail" key={img} />
        )
    }


    album = (item, i) => {
        console.log(item.albumThumbnails)
        return (
        <div className="pure-u-1-4" key={item.id}>
            <figure>
                <Link to={'/album/' + item.id}>

                { item.albumThumbnails ? item.albumThumbnails.map(img => this.albumThumb(img)) : <pre>Loading images...</pre> }

                    <figcaption>
                        <h4>{item.id}: {item.title}</h4>
                        <button className="button-c pure-button">Goto album</button>
                    </figcaption>
                </Link>
            </figure>
        </div>
        )
    }
  

    render() {
        return (
			<div>
            <Header />
            <h1>Photo albums</h1>
            <div className="pure-g">
			{ this.props.data.length ? this.props.data.map(album => this.album(album)) : <pre>Loading albums...</pre> }
			</div>
            </div>
		)
    }

}


const mapStateToProps = state => ({ data: state.data })

const mapDispatchToProps = dispatch => bindActionCreators({ requestAlbums }, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Albums)





