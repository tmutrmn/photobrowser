import { call, put, takeLatest, takeEvery } from 'redux-saga/effects'
import { fork } from 'redux-saga/effects';

import { REQUEST_ALBUMS, RECEIVE_ALBUMS } from '../actions/Actions'
import { REQUEST_ALBUM, RECEIVE_ALBUM } from '../actions/Actions'
import { REQUEST_IMAGE, RECEIVE_IMAGE } from '../actions/Actions'
import { GetAlbums, GetAlbumImages, GetImage } from '../api/Api'    // fetchData



function* getAlbumsData(action) {   // worker saga
	try {
    let data = yield call(GetAlbums);   // do api call
    yield put({ type: RECEIVE_ALBUMS, data })
	} catch (e) {
		console.log(e)
  }
}


function* getAlbumData(action) {   // worker saga
	try {
    const data = yield call(GetAlbumImages, action.albumId);    // do api call
    yield put({ type: RECEIVE_ALBUM, data });
	} catch (e) {
		console.log(e)
  }
}


function* getImageData(action) {   // worker saga
	try {
    const data = yield call(GetImage, action.imageId);    // do api call
    yield put({ type: RECEIVE_IMAGE, data });
	} catch (e) {
		console.log(e)
  }
}


function* mySaga() {    // watcher saga
  yield [
    fork(takeLatest, REQUEST_ALBUMS, getAlbumsData),
    fork(takeLatest, REQUEST_ALBUM, getAlbumData), 
    fork(takeLatest, REQUEST_IMAGE, getImageData), 
  ];
}

export default mySaga;