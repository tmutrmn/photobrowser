import { RECEIVE_ALBUMS } from '../actions/Actions'
import { RECEIVE_ALBUM } from '../actions/Actions'
import { RECEIVE_IMAGE } from '../actions/Actions'


export default (state = {}, { type , data }) => {
	switch (type) {
		case RECEIVE_ALBUMS:
			return data
		case RECEIVE_ALBUM:
			return data
		case RECEIVE_IMAGE:
			return data
		default :
			return state
	}
}