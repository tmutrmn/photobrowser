import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
// import { history } from "./store.js";
import App from "./components/App";
import Home from "./components/Home";
import Albums from "./components/Albums";
import Album from "./components/Album";
import Image from "./components/Image";
import About from "./components/About";
import NotFound from "./components/NotFound";

import { Provider } from 'react-redux'
import store from './store'

// 
const Routes = (
  <Provider store={store}>
  <BrowserRouter>
    <div>
      <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/albums" component={Albums} />
        <Route path="/album/:albumId" component={Album} />
        <Route path="/image/:imageId" component={Image} />
        <Route path="/about" component={About} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </BrowserRouter>
  </Provider>
)

export { Routes };