/*** API calls ***/

export const GetAllImages = () => {
    const API_URL = 'https://jsonplaceholder.typicode.com/photos'
    return fetch(API_URL).then(resp => {
        return resp.json()
    }).then(json => {
        return json.map(({ albumId, id, title, url, thumbnailUrl }) => ({
            albumId,
            id,
            title,
            url,
            thumbnailUrl
        }))
    })
};


export const GetAlbums = () => {
    let data = [];
    const API_URL = 'https://jsonplaceholder.typicode.com/albums'
    return fetch(API_URL)
    .then(response => {
        return response.json()
    })
    .then(json => {
        let albums = json.map(({ userId, id, title }) => ({
            userId,
            id,
            title
        }))
        let promises = [];
        albums.forEach(album => {       // fetch album images for each album
            let fetchPromise = fetch('https://jsonplaceholder.typicode.com/albums/' + album.id + '/photos')
            .then(response => { 
                return response.json();
            }).then(json => {
                //console.log(json[0].thumbnailUrl, json[1].thumbnailUrl, json[2].thumbnailUrl, json[3].thumbnailUrl, json[4].thumbnailUrl, json[5].thumbnailUrl)
                data.push({ userId: album.userId, id: album.id, title: album.title, 
                    albumThumbnails: [json[0].thumbnailUrl, json[1].thumbnailUrl, json[2].thumbnailUrl, json[3].thumbnailUrl, json[4].thumbnailUrl, json[5].thumbnailUrl] })
            })
            promises.push(fetchPromise)
        })
        return Promise.all(promises)
    })
    .then(() => {
        console.log(data)
        data.sort(function(a, b) {
            return a.id - b.id;     // sort by album id
        });
        return data;
    })
}


export const GetAlbumImages = (album) => {
    const API_URL = 'https://jsonplaceholder.typicode.com/albums/' + album + '/photos'
    return fetch(API_URL).then(resp => {
        return resp.json()
    }).then(json => {
        return json.map(({ albumId, id, title, url, thumbnailUrl }) => ({
            albumId,
            id,
            title,
            url,
            thumbnailUrl
        }))
    })
};


export const GetAlbumImage = (album) => {
    const API_URL = 'https://jsonplaceholder.typicode.com/albums/' + album + '/photos'
    return fetch(API_URL).then(resp => {
        return resp.json()
    }).then(json => {
        return json.slice(0,1).map(({ albumId, id, title, url, thumbnailUrl }) => ({
            albumId,
            id,
            title,
            url,
            thumbnailUrl
        }))
    })
};

export const GetImage = (id) => {
    const API_URL = 'https://jsonplaceholder.typicode.com/photos/' + id
    return fetch(API_URL).then(resp => {
        return resp.json()
    })
};
